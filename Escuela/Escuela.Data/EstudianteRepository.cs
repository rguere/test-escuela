﻿using Escuela.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escuela.Data
{
    //Conexiòn a la Base de Datos
  public  class EstudianteRepository
    {
        public static List<Estudiante> GetEstudiantes()  /* Metodo para Obtener la Lista de los Estudiantes*/
        {
            List<Estudiante> list = new List<Estudiante>();

            string connectionString = "Data Source=162.252.57.64,2350;Initial Catalog=Escuela;user id=sa;password=Zeven123;";
            string queryString = "SELECT * from dbo.Estudiante";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                try
                {
                    connection.Open();      /*Abre conexion a la base de datos*/
                    SqlDataReader reader = command.ExecuteReader();/* Ejecuta el query*/
                    while (reader.Read())  /*Recorre la informaciòn de Estudiante*/
                    {

                      //Pasar la Informaciòn Recibida a Tipo Estudiante

                        Estudiante columna = new Estudiante()
                        {
                            Matricula_E = Convert.ToInt32(reader[0]),
                            Nombre_E = reader[1].ToString(),
                            Apellido_E = reader[2].ToString(),
                            Grado_E = reader[3].ToString(),
                            Sexo_E = Convert.ToInt32(reader[4]),
                            Representante_Id = Convert.ToInt32(reader[5])
                        };
                       
                        list.Add(columna);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                return list;
            }
        }


        public static List<Estudiante> GetEstudiantesByRepresentanteId(int IdRe)  /* Metodo para Obtener la Lista de los Estudiantes By Reprensentante*/
        {
            List<Estudiante> list = new List<Estudiante>();

            string connectionString = "Data Source=162.252.57.64,2350;Initial Catalog=Escuela;user id=sa;password=Zeven123;";
            string queryString = "SELECT * from dbo.Estudiante Where Representante_Id = @id";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@id", IdRe);
                try
                {
                    connection.Open();      /*Abre conexion a la base de datos*/
                    SqlDataReader reader = command.ExecuteReader();/* Ejecuta el query*/
                    while (reader.Read())  /*Recorre la informaciòn de Estudiante*/
                    {

                        //Pasar la Informaciòn Recibida a Tipo Estudiante

                        Estudiante columna = new Estudiante()
                        {
                            Matricula_E = Convert.ToInt32(reader[0]),
                            Nombre_E = reader[1].ToString(),
                            Apellido_E = reader[2].ToString(),
                            Grado_E = reader[3].ToString(),
                            Sexo_E = Convert.ToInt32(reader[4]),
                            Representante_Id = Convert.ToInt32(reader[5])
                        };

                        list.Add(columna);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                return list;
            }
        }


        public static Estudiante GetEstudianteById(int Id)  /*Metodo para Obtener la Informaciòn de un Estudiante por ID*/
        {
            Estudiante estudiante = new Estudiante();

            string connectionString = "Data Source=162.252.57.64,2350;Initial Catalog=Escuela;user id=sa;password=Zeven123;";
            string queryString = "SELECT * from dbo.Estudiante where Matricula_E = @id";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                var command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@id", Id);
                connection.Open();

                using (var dr = command.ExecuteReader())
                {               
                    try
                    {                     
                        dr.Read();
                        
                            Estudiante columna = new Estudiante()
                            {
                                Matricula_E = Convert.ToInt32(dr["Matricula_E"]),
                                Nombre_E = dr["Nombre_E"].ToString(),
                                Apellido_E = dr["Apellido_E"].ToString(),
                                Grado_E = dr["Grado_E"].ToString(),
                                Sexo_E = Convert.ToInt32(dr["Sexo_E"]),
                                Representante_Id = Convert.ToInt32(dr["Representante_Id"])
                            };
                        estudiante = columna;
                       
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                return estudiante;
            }
        }

        public static bool Create(Estudiante estudiante) /*Metodo para Insertar Un Estudiante*/
        {          
            bool respuesta = false;
            string connectionString = "Data Source=162.252.57.64,2350;Initial Catalog=Escuela;user id=sa;password=Zeven123;";
            string queryString = "INSERT INTO Estudiante(Nombre_E, Apellido_E, Grado_E, Sexo_E, Representante_Id) VALUES(@e0, @e1, @e2, @e3, @e4)";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                try
                {
                    connection.Open();

                    command.Parameters.AddWithValue("@e0", estudiante.Nombre_E);
                    command.Parameters.AddWithValue("@e1", estudiante.Apellido_E);
                    command.Parameters.AddWithValue("@e2", estudiante.Grado_E);
                    command.Parameters.AddWithValue("@e3", estudiante.Sexo_E);
                    command.Parameters.AddWithValue("@e4", estudiante.Representante_Id);

                    command.ExecuteNonQuery();

                    respuesta = true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    respuesta = false;
                }

                return respuesta;
            }
        }

        public static bool Update(Estudiante estudiante) /*Metodo para Edtitar Un Estudiante*/
        {
            bool respuesta = false;

            try
            {
                string connectionString = "Data Source=162.252.57.64,2350;Initial Catalog=Escuela;user id=sa;password=Zeven123;";
                string queryString = "UPDATE Estudiante SET Nombre_E = @e0, Apellido_E = @e1, Grado_E = @e2, Sexo_E = @e3 WHERE Matricula_E = @e4";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    connection.Open();

                    command.Parameters.AddWithValue("@e0", estudiante.Nombre_E);
                    command.Parameters.AddWithValue("@e1", estudiante.Apellido_E);
                    command.Parameters.AddWithValue("@e2", estudiante.Grado_E);
                    command.Parameters.AddWithValue("@e3", estudiante.Sexo_E);
                    command.Parameters.AddWithValue("@e4", estudiante.Matricula_E);

                    command.ExecuteNonQuery();

                    respuesta = true;
                }
            }
            catch (Exception ex)
            {
                respuesta = false;
            }

            return respuesta;
        }

        public static bool Eliminar(int id) /*Metodo para Eliminar un Estudiante */
        {
            bool respuesta = false;

            try
            {
                string connectionString = "Data Source=162.252.57.64,2350;Initial Catalog=Escuela;user id=sa;password=Zeven123;";
                string queryString = "DELETE FROM Estudiante WHERE Matricula_E = @e0";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    connection.Open();

                    command.Parameters.AddWithValue("@e0", id);
                    command.ExecuteNonQuery();

                    respuesta = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return respuesta;
        }

        public static bool EliminarByRepresentanteID(int id)   /*Metodo para Eliminar Estudiante por Representante ID*/
        {
            bool respuesta = false;

            try
            {
                string connectionString = "Data Source=162.252.57.64,2350;Initial Catalog=Escuela;user id=sa;password=Zeven123;";
                string queryString = "DELETE FROM Estudiante WHERE Representante_Id = @e0";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    connection.Open();

                    command.Parameters.AddWithValue("@e0", id);
                    command.ExecuteNonQuery();

                    respuesta = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return respuesta;
        }
    }
}

