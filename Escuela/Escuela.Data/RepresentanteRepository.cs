﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Escuela.Entities;

namespace Escuela.Data
{
    //Conexiòn de la Base de Datos
   public class RepresentanteRepository
    {
        public static List<Representante>  GetRepresentantes()
        {
            List<Representante> list = new List<Representante>(); 

            string connectionString = "Data Source=162.252.57.64,2350;Initial Catalog=Escuela;user id=sa;password=Zeven123;";
            string queryString = "SELECT * from dbo.Representante";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                try
                {
                    connection.Open(); /*Abre la Conexiòn a la Base de Datos*/
                    SqlDataReader reader = command.ExecuteReader(); /*Ejecuta el query*/
                    while (reader.Read()) /*Recorre la Informaciòn de los Representante*/
                    {
                        //Pasa la Informacion Recibida a Tipo Representante

                        Representante columna = new Representante()
                        {

                            Clave_R = Convert.ToInt32(reader[0]),
                            Cedula_R = Convert.ToInt32(reader[1]),
                            Nombre_R = reader[2].ToString(),
                            Apellido_R = reader[3].ToString(),
                            Sexo_R = Convert.ToInt32(reader[4]),
                            Direccion_R = reader[5].ToString(),
                            Telefono_R = reader[6].ToString(),
                            Correo_R = reader[7].ToString()
                        };
                        list.Add(columna);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
               
                return list;               
            }
        }

        public static Representante GetRepresentanteById(int Id) /*Metodo para obtener la Informcaiòn de un Representante  por ID*/
        {
            Representante representante = new Representante();

            string connectionString = "Data Source=162.252.57.64,2350;Initial Catalog=Escuela;user id=sa;password=Zeven123;";
            string queryString = "SELECT * from dbo.Representante where Clave_R = @id";


            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                var command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@id", Id);
                connection.Open();

                using (var dr = command.ExecuteReader())
                {
                    try
                    {                 
                        dr.Read();

                        Representante columna = new Representante()
                        {

                            Clave_R = Convert.ToInt32(dr["Clave_R"]),
                            Cedula_R = Convert.ToInt32(dr["Cedula_R"]),
                            Nombre_R = dr["Nombre_R"].ToString(),
                            Apellido_R = dr["Apellido_R"].ToString(),
                            Sexo_R = Convert.ToInt32(dr["Sexo_R"]),
                            Direccion_R =dr["Direccion_R"].ToString(),
                            Telefono_R = dr["Telefono_R"].ToString(),
                            Correo_R = dr["Correo_R"].ToString()

                          
                        };
                        representante = columna;
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                return representante;
            }
        }

        public static Representante GetRepresentanteByCedula(int Cedula) /*Metodo para obtener la Informcaiòn de un Representante  por cedula*/
        {
            Representante representante = new Representante();

            string connectionString = "Data Source=162.252.57.64,2350;Initial Catalog=Escuela;user id=sa;password=Zeven123;";
            string queryString = "SELECT * from dbo.Representante where Cedula_R = @Cedula";


            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                var command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@Cedula", Cedula);
                connection.Open();

                using (var dr = command.ExecuteReader())
                {
                    try
                    {
                        dr.Read();

                        Representante columna = new Representante()
                        {

                            Clave_R = Convert.ToInt32(dr["Clave_R"]),
                            Cedula_R = Convert.ToInt32(dr["Cedula_R"]),
                            Nombre_R = dr["Nombre_R"].ToString(),
                            Apellido_R = dr["Apellido_R"].ToString(),
                            Sexo_R = Convert.ToInt32(dr["Sexo_R"]),
                            Direccion_R = dr["Direccion_R"].ToString(),
                            Telefono_R = dr["Telefono_R"].ToString(),
                            Correo_R = dr["Correo_R"].ToString()


                        };
                        representante = columna;
                        dr.Close();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                return representante;
            }
        }

        public static int Create(Representante representante) /*Metodo para Insertar un Representante*/
        {
            int respuesta = 0;
            string connectionString = "Data Source=162.252.57.64,2350;Initial Catalog=Escuela;user id=sa;password=Zeven123;";
            string queryString = "INSERT INTO Representante(Cedula_R, Nombre_R, Apellido_R, Sexo_R, Direccion_R, Telefono_R, Correo_R ) VALUES(@r0, @r1, @r2, @r3, @r4,@r5,  @r6) SELECT SCOPE_IDENTITY()";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                try
                {
                    connection.Open();

                    command.Parameters.AddWithValue("@r0", representante.Cedula_R);
                    command.Parameters.AddWithValue("@r1", representante.Nombre_R);
                    command.Parameters.AddWithValue("@r2", representante.Apellido_R);
                    command.Parameters.AddWithValue("@r3", representante.Sexo_R);
                    command.Parameters.AddWithValue("@r4", representante.Direccion_R);
                    command.Parameters.AddWithValue("@r5", representante.Telefono_R);
                    command.Parameters.AddWithValue("@r6", representante.Correo_R);

                    respuesta = Convert.ToInt32(command.ExecuteScalar());
                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    respuesta = 0;
                }

                return respuesta;
            }
        }

        public static bool Update(Representante representante)/* Metodo para Editar los datos de un Representante*/
        {
            bool respuesta = false;

            try
            {
                string connectionString = "Data Source=162.252.57.64,2350;Initial Catalog=Escuela;user id=sa;password=Zeven123;";
                string queryString = "UPDATE Representante SET Cedula_R = @r0, Nombre_R = @r1, Apellido_R = @r2, Sexo_R = @r3, Direccion_R = @r4, Telefono_R = @r5, Correo_R = @r6  WHERE Clave_R = @r7";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    connection.Open();

                    command.Parameters.AddWithValue("@r0", representante.Cedula_R);
                    command.Parameters.AddWithValue("@r1", representante.Nombre_R);
                    command.Parameters.AddWithValue("@r2", representante.Apellido_R);
                    command.Parameters.AddWithValue("@r3", representante.Sexo_R);
                    command.Parameters.AddWithValue("@r4", representante.Direccion_R);
                    command.Parameters.AddWithValue("@r5", representante.Telefono_R);
                    command.Parameters.AddWithValue("@r6", representante.Correo_R);
                    command.Parameters.AddWithValue("@r7", representante.Clave_R);

                    command.ExecuteNonQuery();

                    respuesta = true;
                }
            }
            catch (Exception ex)
            {
                respuesta = false;
            }

            return respuesta;
        }

        public static bool Eliminar(int id)/* Metodo para Eliminar los Datos de un Representante*/
        {
            bool respuesta = false;

            try
            {
                string connectionString = "Data Source=162.252.57.64,2350;Initial Catalog=Escuela;user id=sa;password=Zeven123;";
                string queryString = "DELETE FROM Representante WHERE Clave_R = @r0";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    connection.Open();

                    command.Parameters.AddWithValue("@r0", id);
                    command.ExecuteNonQuery();

                    respuesta = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return respuesta;
        }

    }
}

