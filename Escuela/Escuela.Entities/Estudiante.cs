﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escuela.Entities
{
  public  class Estudiante
    {
        public int Matricula_E { get; set; }

        [Required  (ErrorMessage = "El Nombre es requerido")]
        [StringLength(50, ErrorMessage = "Maximo 50 digitos")]
        [Display(Name = "Nombre")]
        public string Nombre_E { get; set; }

        [Required(ErrorMessage = "El Apellido es requerido")]
        [StringLength(50, ErrorMessage = "Maximo 50 digitos")]
        [Display(Name = "Apellido")]
        public string Apellido_E { get; set; }

        
        [Required(ErrorMessage = "El Grado es requerido")]
        [Display(Name = "Grado")]
        public string Grado_E { get; set; }

        [Required(ErrorMessage = "El Sexo es requerido")]
        [Display(Name = "Sexo")]
        public int Sexo_E { get; set; }

        public int Representante_Id { get; set; }
    }
}
    
