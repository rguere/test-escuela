﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Escuela.Models
{
    public class Representante_Estudiante
    {

        public int Clave_R { get; set; }

        [Required(ErrorMessage = "La Cedula es requerida")]
        [Display(Name = "Cedula")]
        [Range(9999, 99999999, ErrorMessage = "Minimo 5 caracteres, maximo 8 caracteres")]
        public int Cedula_R { get; set; }

        [Required(ErrorMessage = "El Nombre requerido")]
        [StringLength(50, ErrorMessage = "Maximo 50 digitos")]
        [Display(Name = "Nombre")]
        public string Nombre_R { get; set; }

        [Required(ErrorMessage = "El Apellido es requerido")]
        [StringLength(50, ErrorMessage = "Maximo 50 digitos")]
        [Display(Name = "Apellido")]
        public string Apellido_R { get; set; }

        [Required(ErrorMessage = "El Sexo es requerido")]
        [Display(Name = "Sexo")]
        public int Sexo_R { get; set; }

        [Required(ErrorMessage = "La Dirreción es requerida")]
        [StringLength(250)]
        [Display(Name = "Dirección")]
        public string Direccion_R { get; set; }

        [Required(ErrorMessage = "El telefono es requerido")]
        [Display(Name = "Telefono")]
        [StringLength(11, ErrorMessage = "El telefono debe tener minimo y maximo 11 caracteres")]
        [DataType(DataType.PhoneNumber)]
        public string Telefono_R { get; set; }

        [StringLength(200)]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "El correo no tiene el formato correcto") ]
        [Display(Name = "Correo")]
        [DataType(DataType.EmailAddress)]
        public string Correo_R { get; set; }


        public int Matricula_E { get; set; }

        [Required(ErrorMessage = "El Nombre es requerido")]
        [StringLength(50, ErrorMessage = "Maximo 50 digitos")]
        [Display(Name = "Nombre")]
        public string Nombre_E { get; set; }

        [Required(ErrorMessage = "El Apellido es requerido")]
        [StringLength(50, ErrorMessage = "Maximo 50 digitos")]
        [Display(Name = "Apellido")]
        public string Apellido_E { get; set; }

        [Required(ErrorMessage = "El Grado es requerido")]
        [Display(Name = "Grado")]
        public string Grado_E { get; set; }

        [Required(ErrorMessage = "El Sexo es requerido")]
        [Display(Name = "Sexo")]
        public int Sexo_E { get; set; }
    }
}