﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Escuela.Data;
using Escuela.Entities;

namespace Escuela.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            List<Representante> list = RepresentanteRepository.GetRepresentantes();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}