﻿using Escuela.Data;
using Escuela.Entities;
using Escuela.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Escuela.Controllers
{
    public class FormularioController : Controller
    {
        //Get de la Vista Datos  para Mostrar los datos de los Representantes
        public ActionResult Datos(int Id = 0, int reId = 0)
        {
            List<Representante> list = RepresentanteRepository.GetRepresentantes();
            ViewBag.ListRepresentante = list;
            return View();
        }

        //Get de la Vista Estudiante Para Mostrar Formulario y poder Agregar los datos de un Estudiante
        public ActionResult Estudiante(int Id = 0, int reId = 0)
        {
            Estudiante estudiante = new Estudiante();
            if (Id != 0)
            {
                estudiante = EstudianteRepository.GetEstudianteById(Id);
                return View(estudiante);
            }
            else
            {
                estudiante.Representante_Id = reId;
                return View(estudiante);
            }           
        }

        //Post para Editar Estudiantes
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Estudiante(Estudiante estudiante)
        {
            if (ModelState.IsValid)
            {
                if (estudiante.Matricula_E != 0)
                {
                    bool status = EstudianteRepository.Update(estudiante);
                }
                else
                {
                    bool status = EstudianteRepository.Create(estudiante);
                }
                return RedirectToAction("Mostrar", "Formulario", routeValues: new { idRe = estudiante.Representante_Id });
            }
            else
            {
                return View(estudiante);
            }           
        }

        //Get de la Vista Mostrar  para Ver la Lista de los datos de los Estudiantes
        public ActionResult Mostrar(int idRe)
        {
            List<Estudiante> list = EstudianteRepository.GetEstudiantesByRepresentanteId(idRe);
            ViewBag.ListEstudiante = list;
            return View();
        }

        //Get de la Vista Representante para Mostrar  Formulario y poder Agregar los datos de los Representantes
        public ActionResult Representante(int Id = 0)
        {
            Representante representante = new Representante();      
            representante = RepresentanteRepository.GetRepresentanteById(Id);
            return View(representante);
        }

        //Post para Editar Representatante
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Representante(Representante representante)
        {
            if (ModelState.IsValid)
            {
                if (representante.Correo_R == null)
                {
                    representante.Correo_R = "";
                }

                bool status = RepresentanteRepository.Update(representante);

                return RedirectToAction("Datos", "Formulario");
            }
            else
            {
                return View(representante);
            }         
        }

        public ActionResult Formgeneral()
        {
            return View();
        }

        //Post de la Vista Formgeneral para Mostrar los Datos de los Representante y Estudiantes
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Formgeneral(Representante_Estudiante representante)
        {
            if (ModelState.IsValid)
            {
                Representante representanteV = RepresentanteRepository.GetRepresentanteByCedula(representante.Cedula_R);

                if (representanteV != null && representanteV.Cedula_R == representante.Cedula_R)
                {
                    ModelState.AddModelError("Cedula_R", "Cedula ya existe en Base de Datos");
                    return View(representante);
                }               

                if (representante.Correo_R == null)
                {
                    representante.Correo_R = "";
                }

                Representante columna = new Representante()
                {
                    Cedula_R = representante.Cedula_R,
                    Nombre_R = representante.Nombre_R,
                    Apellido_R = representante.Apellido_R,
                    Sexo_R = representante.Sexo_R,
                    Direccion_R = representante.Direccion_R,
                    Telefono_R = representante.Telefono_R,                    
                    Correo_R = representante.Correo_R
                };

                int IdRe = RepresentanteRepository.Create(columna);

                if (IdRe != 0)
                {
                    Estudiante columna2 = new Estudiante()
                    {

                        Nombre_E = representante.Nombre_E,
                        Apellido_E = representante.Apellido_E,
                        Grado_E = representante.Grado_E,
                        Sexo_E = representante.Sexo_E,
                        Representante_Id = IdRe

                    };

                    bool status = EstudianteRepository.Create(columna2);
                    return RedirectToAction("Datos", "Formulario");
                }
                else
                {
                    return View(representante);
                }
                
            }
            else
            {
                return View(representante);
            }                     
        }

        //Post para eliminar los Datos de un Estudiante por Id
        [HttpPost]
        public ActionResult EliminarEstudiante(int Id)
        {
            bool sucess = false;
            try
            {
                sucess = EstudianteRepository.Eliminar(Id);
                return Json(new { success = sucess }) ;
                
            }
            catch(Exception ex)
            {
                sucess = false;
                return Json(new { success = sucess });
            }
        }

        //Post para eliminar los Datos de un Representante por Id
        [HttpPost]
        public ActionResult EliminarRepresentante(int Id)
        {
            bool sucess = false;
            try
            {
                sucess = EstudianteRepository.EliminarByRepresentanteID(Id);
                sucess = RepresentanteRepository.Eliminar(Id);             
                return Json(new { success = sucess });

            }
            catch (Exception ex)
            {
                sucess = false;
                return Json(new { success = sucess });
            }
        }
    }
}
